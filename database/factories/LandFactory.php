<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $continents=array('Afrique', 'Amérique', 'Europe', 'Asie', 'Océanie', 'Antartique');
        $monnaie=array('XOF','EUR','DOLLAR');
        $langue=array('FR', 'EN', 'AR', 'ES');
        return [
            'libelle' => $this->faker->country(),
            'description' => $this->faker->paragraph(),
            'code_indicatif' => $this->faker->unique()->numerify('+###'),
            'continent' => $continents[array_rand($continents)],
            'population' => rand(500000, 100000). ' habitants',
            'capitale' => $this->faker->city(),
            'monnaie' => $monnaie[array_rand($monnaie)],
            'langue' => $langue[array_rand($langue)],
            'superficie' => rand(500000, 500000). ' km²',
            'est_laique'=>$this->faker->boolean(),
        ];
    }
}

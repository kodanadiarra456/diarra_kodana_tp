@extends('layouts.main')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Formulaire de modification des pays</h3>
        </div>
        <!-- /.card-header -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- form start -->
        <form action="{{ route('lands.update',["id" => $land->id]) }}" method="POST">
            @csrf
            @method('POST')
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Libelle</label>
                    <input type="text" name="libelle" class="form-control {{$errors->has('libelle') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez le libelle..." value="{{ $land['libelle'] }}">
                    @if ($errors->has('libelle'))
                        <div class="alert alert-danger">
                            {{ $errors->first('libelle') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Code indicatif</label>
                    <input type="text" name="code_indicatif" class="form-control {{$errors->has('code_indicatif') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez le code indicatif précédé d'un +..." value="{{ $land['code_indicatif'] }}">
                    @if ($errors->has('code_indicatif'))
                        <div class="alert alert-danger">
                            {{ $errors->first('code_indicatif') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Continent</label>
                    <select name="continent" id="">
                        <option value="Afrique">Afrique</option>
                        <option value="Amérique">Amérique</option>
                        <option value="Europe">Europe</option>
                        <option value="Asie">Asie</option>
                        <option value="Océanie">Océanie</option>
                        <option value="Antartique">Antartique</option>
                    </select>
                </div>
                <label for="exampleInputEmail1">Population</label><br>
                <div class="input-group">
                    <input type="text" name="population" class="form-control {{$errors->has('population') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez la population (habitants)..." value="{{ $land['population'] }}">
                </div>
                @if ($errors->has('population'))
                    <div class="alert alert-danger">
                        {{ $errors->first('population') }}
                    </div>
                @endif <br>
                <div class="form-group">
                    <label for="exampleInputEmail1">Capitale</label>
                    <input type="text" name="capitale" class="form-control {{$errors->has('capitale') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez la capitale..." value="{{ $land['capitale'] }}">
                    @if ($errors->has('capitale'))
                        <div class="alert alert-danger">
                            {{ $errors->first('capitale') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Monnaie</label>
                    <select name="monnaie" id="">
                        <option value="XOF">XOF</option>
                        <option value="EUR">EUR</option>
                        <option value="DOLLAR">DOLLAR</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Langue</label>
                    <select name="langue" id="">
                        <option value="FR">FR</option>
                        <option value="EN">EN</option>
                        <option value="AR">AR</option>
                        <option value="ES">ES</option>
                    </select>
                </div>
                <label for="exampleInputEmail1">Superficie</label>
                <div class="input-group">
                    <input type="text" name="superficie" class="form-control {{$errors->has('superficie') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez la superficie (km^2)..." value="{{ $land['superficie'] }}">
                </div>
                @if ($errors->has('superficie'))
                    <div class="alert alert-danger">
                        {{ $errors->first('superficie') }}
                    </div>
                @endif <br>
                <div class="form-group">
                    <label for="exampleInputEmail1">Est laique ?</label>
                    <select name="est_laique" id="" value="{{ old('est_laique') }}">
                        <option value="1">OUI</option>
                        <option value="0">NON</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    <textarea name="description" class="form-control {{$errors->has('description') ? 'error' : ''}} rounded-0" id="exampleInputPassword1" placeholder="Entrez la description...">{{ $land['description'] }}</textarea>
                    @if ($errors->has('description'))
                        <div class="alert alert-danger">
                            {{ $errors->first('description') }}
                        </div>
                    @endif
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Modifier</button>
            </div>
        </form>
    </div>
@endsection

@extends('layouts.main')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Formulaire d'enregistrement des pays</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('lands.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Libelle</label>
                    <input type="text" name="libelle" class="form-control {{$errors->has('libelle') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez le libelle..." value="{{ old('libelle') }}">
                    @if ($errors->has('libelle'))
                        <div class="alert alert-danger">
                            {{ $errors->first('libelle') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Code indicatif</label>
                    <input type="text" name="code_indicatif" class="form-control {{$errors->has('code_indicatif') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez le code indicatif précédé d'un +..." value="{{ old('code_indicatif') }}">
                    @if ($errors->has('code_indicatif'))
                        <div class="alert alert-danger">
                            {{ $errors->first('code_indicatif') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Continent</label>
                    <select name="continent" id="" value="{{ old('continent') }}">
                        <option value="Afrique">Afrique</option>
                        <option value="Amérique">Amérique</option>
                        <option value="Europe">Europe</option>
                        <option value="Asie">Asie</option>
                        <option value="Océanie">Océanie</option>
                        <option value="Antartique">Antartique</option>
                    </select>
                </div>
                <label for="exampleInputEmail1">Population</label><br>
                <div class="input-group">
                    <input type="number" name="population" class="form-control {{$errors->has('population') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez la population (habitants)..." value="{{ old('population') }}">
                    <span class="input-group-append">
                        <div type="button" class="btn btn-info btn-flat">habitants</div>
                    </span>
                </div>
                @if ($errors->has('population'))
                    <div class="alert alert-danger">
                        {{ $errors->first('population') }}
                    </div>
                @endif <br>
                <div class="form-group">
                    <label for="exampleInputEmail1">Capitale</label>
                    <input type="text" name="capitale" class="form-control {{$errors->has('capitale') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez la capitale..." value="{{ old('capitale') }}">
                    @if ($errors->has('capitale'))
                        <div class="alert alert-danger">
                            {{ $errors->first('capitale') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Monnaie</label>
                    <select name="monnaie" id="" value="{{ old('monnaie') }}">
                        <option value="XOF">XOF</option>
                        <option value="EUR">EUR</option>
                        <option value="DOLLAR">DOLLAR</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Langue</label>
                    <select name="langue" id="" value="{{ old('langue') }}">
                        <option value="FR">FR</option>
                        <option value="EN">EN</option>
                        <option value="AR">AR</option>
                        <option value="ES">ES</option>
                    </select>
                </div>
                <label for="exampleInputEmail1">Superficie</label>
                <div class="input-group">
                    <input type="number" name="superficie" class="form-control {{$errors->has('superficie') ? 'error' : ''}} rounded-0" id="exampleInputEmail1" placeholder="Entrez la superficie (km^2)..." value="{{ old('superficie') }}">
                    <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat">Km²</button>
                    </span>
                </div>
                @if ($errors->has('superficie'))
                    <div class="alert alert-danger">
                        {{ $errors->first('superficie') }}
                    </div>
                @endif <br>
                <div class="form-group">
                    <label for="exampleInputEmail1">Est laique ?</label>
                    <select name="est_laique" id="" value="{{ old('est_laique') }}">
                        <option value="1">OUI</option>
                        <option value="0">NON</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    <textarea name="description" class="form-control {{$errors->has('description') ? 'error' : ''}} rounded-0" id="exampleInputPassword1" placeholder="Entrez la description...">{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                        <div class="alert alert-danger">
                            {{ $errors->first('description') }}
                        </div>
                    @endif
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Ajouter</button>
            </div>
        </form>
    </div>
@endsection

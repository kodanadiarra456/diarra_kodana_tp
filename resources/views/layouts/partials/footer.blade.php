<footer class="main-footer">
    <strong>Copyright &copy; 2014-2021 <a href="mailto:kodanadiarra456@gmail.com">Kodana DIARRA</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0
    </div>
  </footer>

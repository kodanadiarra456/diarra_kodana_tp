@extends('layouts.main')

@section('content')
    <div class="card">
        <div class="card-header">
            <p class="card-title"><span style="font-weight: 900">{{$land['libelle']}}</span> <small class="m-l-sm">{{$land['code_indicatif']}}</small> <a href="{{ route("lands.edit", ["id" => $land->id]) }}"><button class="btn btn-primary btn-circle" type="button"><i class="fa fa-pencil"></i></button></a> <a href="{{ route("lands.destroy", ["id" => $land->id]) }}"><button class="btn btn-danger btn-circle" type="button"><i class="fa fa-trash"></i></button></a></p>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div><span style="font-weight: 700">Continent : </span>{{$land['continent']}}</div>
            <div><span style="font-weight: 700">Population : </span>{{$land['population']}}</div>
            <div><span style="font-weight: 700">Capitale : </span>{{$land['capitale']}}</div>
            <div><span style="font-weight: 700">Monnaie : </span>{{$land['monnaie']}}</div>
            <div><span style="font-weight: 700">Langue : </span>{{$land['langue']}}</div>
            <div><span style="font-weight: 700">Superficie : </span>{{$land['superficie']}}</div>
            <div><span style="font-weight: 700">Est laique : </span>{{($land['est_laique'])}}</div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            {{$land['description']}}
        </div>
        <!-- /.card-footer-->
    </div>
@endsection
